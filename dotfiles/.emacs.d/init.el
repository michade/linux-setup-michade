;;=================================================
;; init.el
;; Author: Michał Denkiewicz (michal.denkiewiz@gmail.com)
;; License: TODO
;; Emacs configurration file (Based on various sources).
;; TODO: modularize this file (skip parts for packages without "requirements" installed)
;;=================================================

;; Debug mode
;; (setq debug-on-error t)


;;=================================================
;; MELPA
;;=================================================

(require 'package)
(setq package-user-dir "~/.emacs.d/local/elpa")
(add-to-list 'package-archives
	     '("melpa" . "http://melpa.org/packages/") t) ;; all packages
(package-initialize)
(when (not package-archive-contents)
  (package-refresh-contents))

(defvar my-melpa-packages
  '(
    better-defaults
    buffer-move
    yasnippet
    ein
    elpy    
    flycheck
    py-autopep8
    jedi-direx
    neotree
    autopair
    auctex
    yaml-mode
    markdown-mode
    web-mode
    json-mode
    cython-mode
    flycheck-cython
    material-theme
    zenburn-theme
    monokai-theme
    multiple-cursors
    pyenv-mode
    pyenv-mode-auto
    realgud
    traad
    which-key
    ))

(mapc #'(lambda (package)
	  (unless (package-installed-p package)
	    (package-install package)))
      my-melpa-packages)


;;=================================================
;; Color theme
;;=================================================

;; (load-theme 'zenburn t)
;; (load-theme 'almost-monokai t)
(load-theme 'material t)


;;=================================================
;; Basics
;;=================================================

;; inhibit startup message
(setq inhibit-startup-message t)

;; window GUI - disable toolbar
(custom-set-variables '(tool-bar-mode nil))

;; line numbering
;; some customization here - dynamic width of line number column
(require 'linum)
(global-linum-mode t)
(defvar my-linum-format-string "%4d ")
(add-hook 'linum-before-numbering-hook 'my-linum-get-format-string)

(defun my-linum-get-format-string ()
  (let* ((width (length (number-to-string
                         (count-lines (point-min) (point-max)))))
         (format (concat "%" (number-to-string width) "d ")))
    (setq my-linum-format-string format)))

(setq linum-format 'my-linum-format)

(defun my-linum-format (line-number)
  (propertize (format my-linum-format-string line-number) 'face 'linum))

;; InteractiveDoThings mode
;; https://www.masteringemacs.org/article/introduction-to-ido-mode
(when (require 'ido)
  (setq ido-save-directory-list-file "~/.emacs.d/local/ido.last")
  (setq ido-enable-flex-matching t)
  (setq ido-everywhere t)
  (ido-mode 1))

;; Uniquify buffer names
(require 'uniquify)

;; Automagically pair parentheses
(when (require 'autopair)
  (autopair-global-mode))

;; Goto-line short-cut key
(global-set-key "\C-l" 'goto-line)

;; Neotree
(when (require 'neotree)
  (global-set-key [f8] 'neotree-toggle))

;; Disable backup
(setq backup-inhibited t)

;; Disable auto save
(setq auto-save-default nil)
(setq auto-save-list-file-prefix nil)

;; Parentheses
(when (require 'paren)
  (show-paren-mode t)
  (setq show-paren-style 'mixed)
  (setq show-paren-delay 0)
  (set-face-background 'show-paren-match (face-background 'default))
  (set-face-foreground 'show-paren-match "#def")
  (set-face-attribute 'show-paren-match nil :weight 'extra-bold))

;; Yasnippets
(require 'yasnippet)
(yas-global-mode 1)

;; compile command under f5
(global-set-key [f5] 'compile)

;; disable line wrapping
(setq-default truncate-lines 1)

;; multiple cursors
(require 'multiple-cursors)
(global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)
;; (global-set-key (kbd "C->") 'mc/mark-next-like-this)
;; (global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
;; (global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)

;; bury compilation buffer if no errors or warning occured
;; https://emacs.stackexchange.com/questions/62/hide-compilation-window
(setq compilation-finish-function
  (lambda (buf str)
    (if (null (string-match ".*exited abnormally.*" str))
        ;;no errors, make the compilation window go away in a few seconds
        (progn
          (run-at-time
           "1 sec" nil 'delete-windows-on
           (get-buffer-create "*compilation*"))
          (message "No Compilation Errors!")))))

;; which-key - key hinting
(require 'which-key)
(which-key-mode)

;;=================================================
;; Window operations
;;=================================================

;; Swaping windows
(require 'buffer-move)
(global-set-key (kbd "<C-S-up>")     'buf-move-up)
(global-set-key (kbd "<C-S-down>")   'buf-move-down)
(global-set-key (kbd "<C-S-left>")   'buf-move-left)
(global-set-key (kbd "<C-S-right>")  'buf-move-right)

;; Switching between windows
(setq windmove-wrap-around nil)
(global-set-key (kbd "<M-up>")       'windmove-up)
(global-set-key (kbd "<M-down>")     'windmove-down)
(global-set-key (kbd "<M-left>")     'windmove-left)
(global-set-key (kbd "<M-right>")    'windmove-right)


;;=================================================
;; Git 
;;=================================================

;;=================================================
;; Python - Elpy
;;=================================================
;; https://github.com/jorgenschaefer/elpy

(when (require 'elpy nil t)
  (elpy-enable)
  ;; (elpy-use-ipython) not mentioned in readthedocs, but elswhere
  (pyvenv-activate "~/.pyenv/versions/elpy-env"))

;; Elpy advanced config

;; Python for RPC
;; (setq elpy-rpc-python-command "python")
;; (setq python-check-command "flake8")

;; Python for console
;; (default):
;; (setq python-shell-interpreter "python"
;;       python-shell-interpreter-args "-i")
(setq python-shell-interpreter "jupyter"
      python-shell-interpreter-args "console --simple-prompt"
      python-shell-prompt-detect-failure-warning nil)
(add-to-list 'python-shell-completion-native-disabled-interpreters
             "jupyter")

;; use flycheck not flymake with elpy
(when (require 'flycheck nil t)
  (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
  (add-hook 'elpy-mode-hook 'flycheck-mode))

;; EIN - Emacs IPython Notebook
;; https://github.com/millejoh/emacs-ipython-notebook
(setq ein:use-auto-complete t)
(setq ein:jupyter-default-server-command "jupyter"
      ein:jupyter-default-server-args "--no-browser"
      ein:jupyter-default-notebook-directory "~/Downloads")
;; TODO: make this a single command
(global-set-key (kbd "C-c C-j") 'ein:notebooklist-login)
(global-set-key (kbd "C-c C-o") 'ein:notebooklist-open)


;; enable autopep8 formatting on save
;; disabled for now
;; (require 'py-autopep8)
;; (add-hook 'elpy-mode-hook 'py-autopep8-enable-on-save)

;; jedi-direx: python directories
;; TODO: not working...
(eval-after-load "python" '(define-key python-mode-map "\C-cx" 'jedi-direx:pop-to-buffer))
(add-hook 'jedi-mode-hook 'jedi-direx:setup)
  
;; Python debugger (pdb) using realgud
(setq realgud:pdb-command-name "python -m pdb")
(setq realgud-safe-mode nil)
(add-hook 'python-mode-hook
	  (lambda ()
	    (require 'realgud)
	    (local-set-key (quote [f6]) (quote realgud:ipdb))))

;; traad server
(setq venv-location "~/.pyenv/versions")
(setq traad-environment-name "elpy-env")
(traad-install-server)

;; cython flycheck plugin
(require 'flycheck-cython)
(add-hook 'cython-mode-hook 'flycheck-mode)

;;=================================================
;; R (ESS)
;;=================================================

; asking for R directory on startup
(setq ess-ask-for-ess-directory nil)

; TODO: needed???
;; (put 'upcase-region 'disabled nil)
;; (put 'downcase-region 'disabled nil)

; disable auto convert '_' to '->'
; to write '_' press it twice
;; (ess-toggle-underscore nil)

; disable Rhistory
(setq inferior-R-args "--no-save --no-restore")


;;=================================================
;; Web mode
;;=================================================
;; http://web-mode.org/


(when (require 'web-mode)
  (add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.jsp\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
  (setq web-mode-engines-alist
      '(("jinja2"    . "\\.html\\'"))
  ))


;;=================================================
;; LaTeX (AUCTeX)
;;=================================================
;; https://www.emacswiki.org/emacs/AUCTeX
;; TODO: configure previews

(setq TeX-PDF-mode t)
(setq TeX-auto-save nil)
(setq TeX-parse-self t)
(setq-default TeX-master nil)
(add-hook 'LaTeX-mode-hook 'visual-line-mode)
(add-hook 'LaTeX-mode-hook 'flyspell-mode)
(add-hook 'LaTeX-mode-hook 'LaTeX-math-mode)
(add-hook 'LaTeX-mode-hook 'turn-on-reftex)
(setq reftex-plug-into-AUCTeX t)

;; For AbbervMode, apparently used by AUCTeX
;; TODO: does not appera to work (prompt still appears)
(setq abbrev-file-name "~/.emacs.d/local/abbrev_defs")


;;=================================================
;; YAML, Markdown, JSON...
;;=================================================

;; yaml
(add-hook 'yaml-mode-hook
	  (lambda () (define-key yaml-mode-map "\C-m" 'newline-and-indent)))

;; json syntax checking
(add-hook 'json-mode-hook #'flycheck-mode)


;;=================================================
;; MISC
;;=================================================

;;; File for the Custom package (so it doesn't append to init.el)
(setq custom-file "~/.emacs.d/local/custom.el")
(load custom-file)
