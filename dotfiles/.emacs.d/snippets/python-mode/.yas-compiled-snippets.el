;;; Compiled snippets and support files for `python-mode'
;;; Snippet definitions:
;;;
(yas-define-snippets 'python-mode
		     '(("fhead" "\"\"\"\n==========================================================================\n\nFollowing code is a part of the PyInteract - library for multi-station psychology\nexperiments, based on capabilities provided by PsychoPy2 (www.psychopy.org).\nCreated for the \"4 interacting minds\" study, conducted by\nThe Institute of Psychology of the Polish Academy of Sciences, 2013.\n\nAuthor: Michal Denkiewicz (michal.denkiewicz@gmail.com)\n\n==========================================================================\n\n$1\n\n\"\"\"" "Comment in file header" nil nil nil nil nil nil)
		       ("xx" "\"\"\"\n$1\n\"\"\"" "comment" nil nil nil nil nil nil)
		       ("xxa" "\"\"\"\n$1\nArguments:\n$2\n\"\"\"" "comment-arg" nil nil nil nil nil nil)
		       ("xxar" "\"\"\"\n$1\nArguments:\n$2\nReturns:\n$3\n\"\"\"" "comment-arg-ret" nil nil nil nil nil nil)
		       ("xxi" "\"\"\"\nInitializes the object.$1\nArguments:\n$2\n\"\"\"" "comment-init" nil nil nil nil nil nil)
		       ("xxr" "\"\"\"\n$1\nReturns:\n$2\n\"\"\"" "comment-ret" nil nil nil nil nil nil)))


;;; Do not edit! File generated at Tue Nov 26 23:40:26 2013
