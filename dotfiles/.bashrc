# Based on file by Balaji S. Srinivasan (balajis@stanford.edu)
#
# Concepts:
#
#    1) .bashrc is the *non-login* config for bash, run in scripts and after
#        first connection.
#    2) .bash_profile is the *login* config for bash, launched upon first connection.
#    3) .bash_profile imports .bashrc, but not vice versa.
#    4) .bashrc imports .bashrc_custom, which can be used to override
#        variables specified here.
#           
# When using GNU screen:
#
#    1) .bash_profile is loaded the first time you login, and should be used
#       only for paths and environmental settings

#    2) .bashrc is loaded in each subsequent screen, and should be used for
#       aliases and things like writing to .bash_eternal_history (see below)
#
# Do 'man bashrc' for the long version or see here:
# http://en.wikipedia.org/wiki/Bash#Startup_scripts
#
# When Bash starts, it executes the commands in a variety of different scripts.
#
#   1) When Bash is invoked as an interactive login shell, it first reads
#      and executes commands from the file /etc/profile, if that file
#      exists. After reading that file, it looks for ~/.bash_profile,
#      ~/.bash_login, and ~/.profile, in that order, and reads and executes
#      commands from the first one that exists and is readable.
#
#   2) When a login shell exits, Bash reads and executes commands from the
#      file ~/.bash_logout, if it exists.
#
#   3) When an interactive shell that is not a login shell is started
#      (e.g. a GNU screen session), Bash reads and executes commands from
#      ~/.bashrc, if that file exists. This may be inhibited by using the
#      --norc option. The --rcfile file option will force Bash to read and
#      execute commands from file instead of ~/.bashrc.

# Source global definitions
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

# 256 color terminal - mainly for Emacs's sake - MD
export TERM=xterm-256color

# ---------------------------------------------------------
# Set up bash prompt and ~/.bash_eternal_history
# ---------------------------------------------------------

## Git prompt
if [ -f ~/.scripts/.git-prompt.sh ]; then
   source ~/.scripts/.git-prompt.sh
fi

function virtualenv_info() {
    # Get Virtual Env
    if [[ -n "$VIRTUAL_ENV" ]]; then
        # Strip out the path and just leave the env name
        venv="${VIRTUAL_ENV##*/}"
    else
        # In case you don't have one activated
        venv=''
    fi
    [[ -n "$venv" ]] && echo "($venv)"
}

# disable the default virtualenv prompt change
export VIRTUAL_ENV_DISABLE_PROMPT=1

# Make prompt informative
# See:  http://www.ukuug.org/events/linux2003/papers/bash_tips/
PS1='\[\033[0;34m\]\u@\h:\[\033[1;34m\]\w\[\033[0;35m\]$(virtualenv_info)\[\033[0;32m\]$(__git_ps1 "[%s]")\[\033[1;34m\]$\[\033[0m\]'

# Do not check for similar commands:
unset command_not_found_handle

# Append to history
# See: http://www.tldp.org/HOWTO/Bash-Prompt-HOWTO/x329.html
shopt -s histappend

## -----------------------
## Set up aliases
## -----------------------

# Listing, directories, and motion
alias ll="ls -alFh --color"
alias lt="ls -alrthF --color"
alias la="ls -A"
alias l="ls -CF"
alias ..="cd .."
alias md="mkdir"
alias du='du -ch --max-depth=1'
alias treeacl="tree -A -C -L 6"

# Text and editor commands
alias em="emacs -nw"     # No X11 windows
alias eg="run-in-nohup.sh emacs"
alias eqq="emacs -nw -Q" # No config and no X11
alias Rv="R --no-save --no-restore"
export EDITOR="emacs -nw"
export VISUAL="emacs -nw"

# grep options
export GREP_COLORS="mt=01;31:fn=34"
alias grep="grep --color=auto"

# sort options
# Ensures cross-platform sorting behavior of GNU sort.
# http://www.gnu.org/software/coreutils/faq/coreutils-faq.html#Sort-does-not-sort-in-normal-order_0021
# setting locale to posix messes things up, esp. latex... so f**k this s**t
# unset LANG
# export LC_ALL=POSIX

# git
alias gis="git status"
alias gia="git add"
alias gic="git commit -m"

## Bash git completion
if [ -f ~/.scripts/.git-completion.bash ]; then
   source ~/.scripts/.git-completion.bash
fi

## R environment
export R_ENVIRON=~/.Renviron

# Configure PATH
#  - These are line by line so that you can kill one without affecting the others.
#  - Lowest priority first, highest priority last.
# (Moved from bash_profile)
export PATH=$PATH:$HOME/.scripts
# pyenv
export PYENV_ROOT=$HOME/.pyenv
export PATH=$PYENV_ROOT/bin:$PATH

## nVidia CUDA
# export PATH=/usr/local/cuda/bin:$PATH
# export LD_LIBRARY_PATH=/usr/local/cuda/lib64
# export CUDA_HOME=/usr/local/cuda

# Theano
# export MKL_THREADING_LAYER=GNU

if command -v pyenv 1>/dev/null 2>&1
then
    eval "$(pyenv init -)"
    eval "$(pyenv virtualenv-init -)"
fi

# pyenv ML environment activation/deactivation scritps
alias pyenv_activate_ml="source pyenv_activate_ml.sh"
alias pyenv_deactivate_ml="source pyenv_deactivate_ml.sh"


## ------------------------------
## User-customized code
## ------------------------------

## Define any user-specific variables you want here.
source ~/.bashrc_custom
