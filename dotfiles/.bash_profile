# .bash_profile file
# Based on file by By Balaji S. Srinivasan (balajis@stanford.edu)

# Factor out all repeated profile initialization into .bashrc
#  - All non-login shell parameters go there
#  - All declarations repeated for each screen session go there
if [ -f ~/.bashrc ]; then
   source ~/.bashrc
fi

# TODO: what is this for?
# touch $HOME/bash_profile_x

# Configure PATH
#  - These are line by line so that you can kill one without affecting the others.
#  - Lowest priority first, highest priority last.
# TODO: why here, and not in .bashrc?
# export PATH=$PATH:~/scripts

# User specific environment and startup programs

PATH=$PATH:$HOME/.local/bin:$HOME/bin

export PATH
