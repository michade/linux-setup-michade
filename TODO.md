1. Cleanup/organize
2. Some global config system (most notably for user name/email)
3. Make emacs modules optional
4. Configurable bash prompt (move to another repo?)
5. Emacs auctex - configure previewing and other stuff
6. Make two emacs config - one "Development" (slow, big), one "fast"
7. Make 'eqq' emacs not create backups
8. Fix emacs EIN vs window move bindings clash
9. Docker setup need logout to confirm user permissions
10.Emacs keeps saving 'ac-comphist.dat' and 'request' in '.emacd.d' (jupyter?)
11.Set emacs as git merge tool
12.Create empty custom.el 
13.Docker prems?
14.Docker stable repo?
15. CUDA: install video drivers
