#!/bin/bash

source ./bash-utils.sh

$cmdinstall keepass2
# for keepasshttp:
$cmdinstall mono-complete

sudo wget -nv https://raw.github.com/pfn/keepasshttp/master/KeePassHttp.plgx -O /usr/lib/keepass2/Plugins/KeePassHttp.plgx
sudo chmod 644 /usr/lib/keepass2/Plugins/KeePassHttp.plgx
