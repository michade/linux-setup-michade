#!/bin/bash

source ./bash-utils.sh

pushd ..

link_and_backup ~/.bashrc "$PWD"/dotfiles/.bashrc
link_and_backup ~/.bash_profile "$PWD"/dotfiles/.bash_profile
link_and_backup ~/.bashrc_custom "$PWD"/dotfiles/.bashrc_custom
# TODO: casuses extra link to show up after 2nd run - fix
# TODO: change scripts dir to sth more appropriate /usr...?
link_and_backup ~/.scripts "$PWD"/scripts

popd

source ~/.bash_profile  # update PATH
