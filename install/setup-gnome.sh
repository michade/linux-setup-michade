#!/bin/bash

source ./bash-utils.sh

# Useful nautilus/gnome stuff

$cmdinstall lm-sensors \
	    gnome-tweak-tool \
	    dconf-editor \
            chrome-gnome-shell
