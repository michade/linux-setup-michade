#!/bin/bash

source ./bash-utils.sh

# jupyter kernel:
# https://irkernel.github.io/installation/

R --vanilla <<< "
pkg.to.install = c(
  'repr',
  'IRdisplay',
  'evaluate',
  'crayon',
  'pbdZMQ',
  'devtools',
  'uuid',
  'digest'
)

r.repo <- 'https://cloud.r-project.org/'
r.libs.dir <- '~/R_libs'

ensure.package <- function (pkg.name, ...) {
    if(require(pkg.name, character.only=T)){
        print(sprintf('Package %s is already installed.', pkg.name))
    } else {
        install.packages(pkg.name, ...)
	print(sprintf('Installing %s', pkg.name))
        if(require(pkg.name, character.only=T)) {
	    print(sprintf('Package %s installed.', pkg.name))
        } else {
            print(sprintf('Could not install package %s.', pkg.name))
        }
    }
}

for (pkg.name in pkg.to.install) {
    ensure.package(pkg.name, repos=r.repo, dependencies=T, lib=r.libs.dir)
}

devtools::install_github('IRkernel/IRkernel')
IRkernel::installspec()
"
