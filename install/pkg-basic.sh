#!/bin/bash

source ./bash-utils.sh

# Basic packages

$cmdinstall ssh \
            curl \
	    wget \
	    tree \
	    rar \
	    screen \
	    nano \
	    htop \
	    zip \
	    unzip \
	    rar \
	    unrar \
	    p7zip-full \
	    cmake
