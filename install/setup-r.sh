#!/bin/bash

source ./bash-utils.sh

# http://www.jason-french.com/blog/2013/03/11/installing-r-in-linux/
# https://www.stat.osu.edu/computer-support/mathstatistics-packages/installing-r-libraries-locally-your-home-directory

if [ ! -d ~/.R_libs ]; then
    mkdir ~/R_libs
fi

pushd ..
link_and_backup ~/.Renviron "$PWD"/dotfiles/.Renviron
popd

# Install R and some needed stuff
$cmdinstall r-base libcairo2-dev

# packages
R --vanilla <<< "
pkg.to.install = c(
  'e1071',
  'data.table',
  'dplyr',
  'car',
  'zoo',
  'xts',  
  'tikzDevice',
  'ggplot2',
  'gridExtra',
  'gdata',
  'reshape2',
  'outliers',
  'lme4',
  'nlme',
  'boot',
  'foreign',
  'svglite'
)

r.repo <- 'https://cloud.r-project.org/'
r.libs.dir <- '~/R_libs'

ensure.package <- function (pkg.name, ...) {
    if(require(pkg.name, character.only=T)){
        print(sprintf('Package %s is already installed.', pkg.name))
    } else {
        install.packages(pkg.name, ...)
	print(sprintf('Installing %s', pkg.name))
        if(require(pkg.name, character.only=T)) {
	    print(sprintf('Package %s installed.', pkg.name))
        } else {
            print(sprintf('Could not install package %s.', pkg.name))
        }
    }
}

for (pkg.name in pkg.to.install) {
    ensure.package(pkg.name, repos=r.repo, dependencies=T, lib=r.libs.dir)
}
"

# left-out packages:
# 'Cairo',  - needs dependecies (cairo pkg)
# 'devtools', - needes dependecies (RCurl + libcurl)
# 'doParallel',
# 'doRNG',
# 'devtools',
# 'pwr',
# 'coin',
# 'knitr',
# 'plotrix'
# 'xlsx',
# 'openxlsx'
