#!/bin/bash

source ./bash-utils.sh

pipinstall="pip install"

# setup the pyenv for elpy rpc
ELPY_PYTHON_VERSION=3.6.5
ELPY_VENV=elpy-env

echo "Using Python version $ELPY_PYTHON_VERSION for Emacs elpy"

# TODO: make na option to force this
if [ -z "$(pyenv virtualenvs | grep $ELPY_VENV)" ]; then
    if [ "$ELPY_PYTHON_VERSION" != "system"  ] && [ -z "$(pyenv versions | grep $ELPY_PYTHON_VERSION)" ]; then
	pyenv install $ELPY_PYTHON_VERSION
    fi

    # TODO: why have to do this?
    eval "$(pyenv init -)"
    eval "$(pyenv virtualenv-init -)"
    
    pyenv virtualenv $ELPY_PYTHON_VERSION $ELPY_VENV
    pyenv activate $ELPY_VENV

    # Either rope or jedi
    #$pipinstall rope
    $pipinstall jedi
    # flake8 for code checks
    $pipinstall flake8
    # and autopep8 for automatic PEP8 formatting
    $pipinstall autopep8
    # and yapf for code formatting
    $pipinstall yapf
    # jupyter for better interactive console
    $pipinstall jupyter
    # rope for refactoring
    $pipinstall rope

    pyenv deactivate
fi

### actions outside python environment

# BIN_PYMACS=/usr/share/emacs/site-lisp
# sudo cp $SRC_PYMACS/pymacs/pymacs.el $BIN_PYMACS/
# emacs -batch -eval "(byte-compile-file \"$BIN_PYMACS/pymacs.el\")"

# # error checking - flymake, pyflakes, pep8
# if [ ! -f dotfiles/.emacs.d/manual-imports/flymake-cursor.el ]; then
#     wget http://www.emacswiki.org/emacs/download/flymake-cursor.el -P dotfiles/.emacs.d/manual-imports/
# fi
# sudo ln -s $HOME/.emacs.d/pycheckers /usr/local/bin/pycheckers

# # directory listing
# if [ ! -f dotfiles/.emacs.d/manual-imports/jedi-direx.el ]; then
#     wget https://raw.github.com/tkf/emacs-jedi-direx/master/jedi-direx.el -P dotfiles/.emacs.d/manual-imports/
# fi

# # python documentation for pydoc-info
# pushd /usr/share/info
# sudo wget https://bitbucket.org/jonwaltman/pydoc-info/downloads/python.info.gz
# sudo install-info --info-dir=/usr/share/info python.info.gz #TODO: wrong install-info?
# popd 


# byte-compile custom directory
# TODO: do this
# pushd $HOME/.emacs-d/manual-imports/
# make
# popd
