#!/bin/bash

source ./bash-utils.sh

# TODO: install video drivers?

# TODO: has to add something to ppa first? probably the same as for video drivers
$cmdinstall nvidia-cuda-toolkit

# If docker is installled - install nvidia-docker
# If you have nvidia-docker 1.0 installed: we need to remove it and all existing GPU containers
if [ $(command -v docker) ]; then
    # script from https://github.com/NVIDIA/nvidia-docker
    docker volume ls -q -f driver=nvidia-docker | xargs -r -I{} -n1 docker ps -q -a -f volume={} | xargs -r docker rm -f
    sudo apt-get purge -y nvidia-docker

    # Add the package repositories
    curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | \
	sudo apt-key add -
    distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
    curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | \
	sudo tee /etc/apt/sources.list.d/nvidia-docker.list
    sudo apt-get update

    # Install nvidia-docker2 and reload the Docker daemon configuration
    $cmdinstall nvidia-docker2
    sudo pkill -SIGHUP dockerd

    # Test nvidia-smi with the latest official CUDA image
    docker run --runtime=nvidia --rm nvidia/cuda nvidia-smi
fi
