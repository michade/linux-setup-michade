#!/bin/bash

source ./bash-utils.sh

# https://docs.docker.com/install/linux/docker-ce/ubuntu/
# remove old versions
sudo apt-get -y remove docker docker-engine docker.io

# Install packages to allow apt to use a repository over HTTPS:
$cmdinstall apt-transport-https \
	    ca-certificates \
	    curl \
	    software-properties-common

# Add Docker’s official GPG key:
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# Set up the stable repository
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

# Install Docker CE
sudo apt-get update -qq
$cmdinstall docker-ce
# required for nvidia plugin:
$cmdinstall docker.io

# Steps to manage Docker as a non-root user
# TODO: needs logout :/
# sudo groupadd docker # done by docker.io
sudo usermod -aG docker $USER

# set docker service to start on boot
sudo systemctl enable docker
