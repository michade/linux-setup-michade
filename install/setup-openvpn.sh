#!/bin/bash

source ./bash-utils.sh

$cmdinstall openvpn \
	    network-manager-openvpn-gnome \
	    ca-certificates

# Nordvpn config
pushd /etc/openvpn
sudo wget -q https://downloads.nordcdn.com/configs/archives/servers/ovpn.zip -O ovpn.zip \
    && sudo unzip ovpn.zip
    sudo rm ovpn.zip
popd

sudo service network-manager restart

echo "5s sleep for network manager to recover"
sleep 5s
echo "Done"
