#!/bin/bash

source ./bash-utils.sh

anaconda_version=anaconda3-5.1.0
envname=sci

# create and activate virtualenv
pyenv install $anaconda_version
pyenv virtualenv $anaconda_version $envname

sci-pyenv

# basic packages
conda install -y \
      matplotlib \
      numpy \
      pandas \
      scipy \
      seaborn \
      jupyter \
      nltk \
      xlrd \
      pytables \
      cython

# deactivate virtualenv
sci-pyenv-deactivate
