#!/bin/bash

cmdinstall="sudo apt install -y"

link_and_backup() {
    from=$1
    to=$2
    if [ "$(readlink $from)" != "$to" ]; then
	printf "Creating link %s -> %s\n" $from $to
	ln -sT --backup=simple --suffix=_backup $to $from
    else
	printf "Link %s -> %s already exists\n" $from $to	
    fi
    return $?
}
