#!/bin/bash

source ./bash-utils.sh

## Build-essential

# gcc, make, it
$cmdinstall build-essential

# current kernel header files
$cmdinstall "linux-headers-$(uname -r)"
