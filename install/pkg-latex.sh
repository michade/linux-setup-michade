#!/bin/bash

source ./bash-utils.sh

# Preliminaries
$cmdinstall ghostscript

# Install LaTeX
# TODO: get rid of docs
$cmdinstall texlive-latex-extra

# some installation tool
$cmdinstall texlive-xetex \
	    latexmk

# Languages (not sure if they are in the pkg above)
$cmdinstall texlive-lang-english \
	    texlive-lang-polish

# Additional stuff
$cmdinstall texlive-science \
	    texlive-pstricks \
	    texlive-pictures \
	    preview-latex-style

# Rubber - automatically recompiles until all references are ok
$cmdinstall rubber
