#!/bin/bash

cmdinstall="sudo apt install -y"

## Git

# Install git - probably done by now...
$cmdinstall git

# Useful git extension for project management
$cmdinstall git-flow

# Update submodules
git submodule init
git submodule update

git config --global color.ui true
git config --global user.name "Michał Denkiewicz"
git config --global user.email "michal.denkiewicz@gmail.com"
git config --global core.editor emacs -nw
git config --global merge.tool emacs -nw
