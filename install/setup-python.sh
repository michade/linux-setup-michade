#!/bin/bash

source ./bash-utils.sh

pipinstall="pip install"

# install Python 3 build dependecies
$cmdinstall libpython3-dev
$cmdinstall zlib1g-dev \
	    libbz2-dev \
	    libssl-dev \
	    libncurses-dev \
	    libsqlite3-dev \
	    libreadline-dev

# TODO: what is this?
# sudo sed -i '/^#\sdeb-src /s/^#//' "/etc/apt/sources.list"
# sudo apt update
# sudo apt-get -y build-dep python3

if [ ! -d ~/.pyenv ]; then
    # first, setup pyenv
    git clone https://github.com/pyenv/pyenv.git ~/.pyenv    
    # and virtualenv extension
    git clone https://github.com/yyuu/pyenv-virtualenv.git ~/.pyenv/plugins/pyenv-virtualenv
fi

source ~/.bashrc
