#!/bin/bash

source ./bash-utils.sh

# emacs
$cmdinstall emacs
# polish dictionary for spellchecking
$cmdinstall aspell aspell-pl

# elisp sources
# $cmdinstall emacs-el

# documentation
# $cmdinstall emacs-common-non-dfsg

# some el-get recipes operate trough mercurial (hg)
# $cmdinstall mercurial

pushd ..
link_and_backup ~/.emacs.d "$PWD"/dotfiles/.emacs.d
popd
