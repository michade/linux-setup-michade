#!/bin/bash

cmdtorun=$1
shift
nohup $cmdtorun $@ > /dev/null 2>&1 &
